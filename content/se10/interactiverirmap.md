+++
title = "Static Element #10"
+++

This is the Interactive RIR Map for the 5 Registries

<p>Regional Internet Registries (RIRs) are  nonprofit corporations that administer and register Internet Protocol (IP)  address space and Autonomous System (AS) numbers within a <a href="https://www.nro.net/about-the-nro/list-of-country-codes-and-rirs-ordered-by-country-code" class="external">defined  region</a>. RIRs also work together on joint projects. </p>

Here goes the interactive map:

{{< se10 >}}