+++
title = "Static Element #4"
+++

Anchor Links

<h3>Table of Contents</h3>
<ul>
  <li><a href="#directory">Directory Services</a></li>
  <li><a href="#manage">Resource Management Services</a></li>
  <li><a href="#routing">Routing Registry</a></li>
  <li><a href="#ote">Operational Test and Evaluation environment (OT&amp;E)</a></li>
  <li><a href="#collab">Community Software Repository</a></li>
</ul>
<h3><a name="directory" id="directory"></a>Directory Services</h3>
<h4><span class="casefix">Whois</span>-RWS</h4>
<p><a href="/resources/whoisrws/index.html">ARIN’s Whois RESTful Web Service (Whois-RWS)</a>, much like the  former Whois, is the directory service for accessing registration data  contained within ARIN’s registration database. <a href="http://whois.arin.net/ui" class="external">Whois-RWS</a> can easily be  integrated into command line scripts similar to Whois or RWhois, or it can be  used with a web browser.</p>
<p>If you wish to use a Command-Line Interface (CLI) to access ARIN’s Whois Service, view our <a href="/resources/services/whois_guide.html#query">Quick Guide to ARIN’s Whois</a>  to learn more about how to customize your searches.</p>
<h4>Registry Data Access Protocol (RDAP)</h4>
<p>RDAP  is a specification from the Internet Engineering Task Force (IETF) designed as a replacement for Whois data services for Domain Name Registries (DNRs) and Regional Internet Registries (RIRs). Unlike Whois, RDAP is an HTTP-based Representational State Transfer (REST)-style protocol with responses  specified in JSON. Whois is a text based protocol, utilizing a specialized protocol and port. Whois defines no queries or responses, and as a result the interaction with DNRs and RIRs can vary significantly.</p>
<p>RDAP is specified in RFCs 7480, 7481, 7482, 7483, 7484, and 7485.</p>
<p>For more information, see the <a href="/resources/rdap.html">RDAP page</a>.</p>
<h4>Bulk <span class="casefix">Whois</span> </h4>
<p><a href="/resources/request/bulkwhois.html">Bulk Whois</a> is a service by which ARIN provides access to a bulk copy of all objects in the ARIN Whois directory service to academic researchers and network operators wishing to view the data for the specific purposes outlined in the &quot;Bulk Whois Terms of Use&quot; (ToS) document. This service is covered by an acceptable use policy, which is detailed in the request form.</p>
<h4>Legacy Number Resources</h4>
<p>ARIN  provides certain registration services for  <a href="/resources/legacy/services.html">legacy number resources</a> without requiring legacy resource holders to enter into a registration services agreement or pay service fees.</p>
<h4>WhoWas </h4>
<p>The <a href="/resources/whowas/index.html">WhoWas service</a> provides historical registration information for a given IP address or ASN. This service is available by request in the Downloads &amp; Services section of ARIN Online. Requests are subject to approval and are covered by a Terms of Use agreement. </p>
<h4>Number Resource Fraud Reporting</h4>
<p>This <a href="/resources/fraud/index.html">fraud reporting process</a> is to be used to notify ARIN of suspected Internet number resource fraud including the submission of falsified utilization or organization information, unauthorized changes to records within ARIN's database, hijacking of number resources in ARIN's database, or fraudulent transfers.</p>
<h4><a name="whoisinaccuracy" id="whoisinaccuracy"></a>Whois Inaccuracy Reporting</h4>
<p>This service is to be used to notify ARIN of inaccurate Whois records, including Point of Contact records (POCs), Organization Identifiers (Org IDs), Network records (NETs), and Autonomous System Numbers (ASNs) with outdated or erroneous organization or contact information. Upon receipt of the notification of inaccurate Whois data, ARIN staff will attempt to contact the organization associated with the inaccurate Whois entry and request updated information. To report a Whois Inaccuracy, select &quot;Whois Inaccuracy Report&quot; from the right menu on any <a href="http://whois.arin.net/ui">whois.arin.net</a> page.</p>

