+++
title = "Static Element #6"
+++

Inline Images

<h3 id="ipv4">How is IPv6 Different than IPv4?</h3>
<p>IPv6 differs from IPv4 in many ways, including address size, format, notation, and possible combinations. We've created a <a href="/knowledge/deploying_ipv6/">quick video</a> to highlight some of the differences. </p>
<p>An IPv6 address consists of 128 bits (as opposed to the 32-bit size of IPv4 addresses) and is expressed in hexadecimal notation. The IPv6 anatomy graphic below represents just one possible configuration of an IPv6 address, although there are many different possibilities. </p>
[<img src="/img/v6_anatomy.png">](/img/v6_anatomy.png) 
<h4>Determining the Netmask and Gateway of an IPv6 Address</h4>
<p>As with IPv4, in IPv6 there is no way to definitively calculate the netmask and gateway using only a given address. Both are established when a person sets up a network, and you would need to contact your network administrator to determine what they are. However, when given an address and a prefix, one can compute the starting and ending addresses of a subnet, just like in IPv4.
<p>To conform to typical conventions about IPv6 addressing of network interfaces, most networks use a /64 prefix. This prefix length  accommodates stateless address autoconfiguration (SLAAC). Note that the length of a given IPv6 network prefix cannot be shorter than the registered IPv6 allocation or assignment.
<p>There is no strong convention as to where to number the gateway; although choosing the smallest number in the network is common.